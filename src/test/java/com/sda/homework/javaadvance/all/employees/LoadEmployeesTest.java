package com.sda.homework.javaAdvance.all.employees;

import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class LoadEmployeesTest {

    @Test
    public void load() {
        //given
        Employee employee1 = new Programmer("Raluca",2500, "java");
        Employee employee2 = new SysAdmin("Mihai",9500, Level.L3);
        File file = new File("dir1/test.txt");
        //when
        List listTest = LoadEmployees.load(file);

        //then
        assertThat(listTest.contains(employee1));
        assertThat(listTest.contains(employee2));
    }
}