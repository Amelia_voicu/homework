package com.sda.homework.designpatterns.decorator;

public class SavingAccount extends AbstractAccount {

    public SavingAccount(double balance) {
        super.balance = balance;
    }

    @Override
    public void verifyAccount() {
        System.out.println("Saving Account");
    }


}
