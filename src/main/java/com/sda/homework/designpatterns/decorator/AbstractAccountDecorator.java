package com.sda.homework.designpatterns.decorator;

public abstract class AbstractAccountDecorator extends AbstractAccount{
    AbstractAccount decoratedAccount;

    public AbstractAccountDecorator(AbstractAccount abstractAccount) {
        this.decoratedAccount = abstractAccount;
    }

    @Override
    public void verifyAccount(){
        decoratedAccount.verifyAccount();
    }

}
