package com.sda.homework.designpatterns.decorator;

public abstract class AbstractAccount {
    double balance;

    public abstract void verifyAccount();
}
