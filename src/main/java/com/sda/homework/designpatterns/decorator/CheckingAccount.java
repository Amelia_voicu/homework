package com.sda.homework.designpatterns.decorator;

public class CheckingAccount extends AbstractAccount {
    private double overdraft;

    public CheckingAccount(double balance, double overdraft) {
        super.balance = balance;
        this.overdraft = overdraft;
    }

    public double getOverdraft() {
        return overdraft;
    }

    public void setOverdraft(double overdraft) {
        this.overdraft = overdraft;
    }

    @Override
    public void verifyAccount() {
        System.out.println("Checking Account");
    }
}
