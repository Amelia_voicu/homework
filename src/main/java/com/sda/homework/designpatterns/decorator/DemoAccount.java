package com.sda.homework.designpatterns.decorator;

import java.sql.SQLOutput;

public class DemoAccount {
    public static void main(String[] args) {

        AbstractAccount account1 = new SavingAccount(100);
        AbstractAccount account2 = new CheckingAccount(100,40);
        account1.verifyAccount();
        account2.verifyAccount();

        System.out.println("**************");
        AbstractAccount account1Sms = new SmsManageableAccountDecorator(account1);
        account1Sms.verifyAccount();

    }
}
