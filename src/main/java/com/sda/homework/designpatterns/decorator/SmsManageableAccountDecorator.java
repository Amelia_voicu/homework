package com.sda.homework.designpatterns.decorator;

public class SmsManageableAccountDecorator extends AbstractAccountDecorator {

    public SmsManageableAccountDecorator(AbstractAccount account) {
        super(account);
    }

    @Override
    public void verifyAccount(){
        sendSmsToCustomer(decoratedAccount);
        decoratedAccount.verifyAccount();
    }

    private void sendSmsToCustomer(AbstractAccount account){
        System.out.println("Contul are optiunea de trimitere sms inclusa");
    }
}
