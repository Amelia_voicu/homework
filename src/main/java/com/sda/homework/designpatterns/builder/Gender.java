package com.sda.homework.designpatterns.builder;

public enum Gender {
    MASCULIN,
    FEMININ;
}
