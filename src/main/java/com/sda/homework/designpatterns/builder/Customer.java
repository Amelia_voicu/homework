package com.sda.homework.designpatterns.builder;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Customer {

    private final Gender gender;
    private final String surname;
    private final String firstName;
    private final String middleName;
    private final Date birthday;
    private final Date becomeCustomer;

    private Customer(CustomerBuilder builder) {
        this.gender = builder.gender;
        this.surname = builder.surname;
        this.firstName = builder.firstName;
        this.middleName = builder.middleName;
        this.birthday = builder.birthday;
        this.becomeCustomer = builder.becomeCustomer;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Date getBecomeCustomer() {
        return becomeCustomer;
    }

    @Override
    public String toString() {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String s = "Customer details: " +
                "  Gender - " + this.gender +
                "   Name - " + this.surname +
                "   FirstName - " + this.firstName;
        if (this.middleName != null)
            s += "   MiddleName - " + this.middleName;
        if (this.birthday != null)
            s += "   Birthday - " + simpleDateFormat.format(this.birthday);
        if (this.becomeCustomer != null)
            s += "   Date when become customer - " + simpleDateFormat.format(this.becomeCustomer);
        return s;
    }

    public static class CustomerBuilder {

        private Gender gender;
        private String surname;
        private String firstName;
        private String middleName;
        private Date birthday;
        private Date becomeCustomer;

        public CustomerBuilder(Gender gender, String surname, String firstName) {
            this.gender = gender;
            this.surname = surname;
            this.firstName = firstName;
        }

        public CustomerBuilder setMiddleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public CustomerBuilder setBirthday(Date birthday) {
            this.birthday = birthday;
            return this;
        }

        public CustomerBuilder setBecomeCustomer(Date becomeCustomer) {
            this.becomeCustomer = becomeCustomer;
            return this;
        }

        public Customer buildCustomer() {
            return new Customer(this);
        }
    }
}
