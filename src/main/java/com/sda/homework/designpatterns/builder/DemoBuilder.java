package com.sda.homework.designpatterns.builder;

import java.util.Date;

public class DemoBuilder {

    public static void main(String[] args) {

        Customer c1 = new Customer.CustomerBuilder(Gender.FEMININ, "Ionescu", "Cristina")
                .buildCustomer();

        Customer c2 = new Customer.CustomerBuilder(Gender.MASCULIN, "Velea", "Mihai")
                .setMiddleName("Alex")
                .setBirthday(new Date(88, 8, 21))
                .buildCustomer();

        Customer c3 = new Customer.CustomerBuilder(Gender.MASCULIN, "Popescu", "Ramon")
                .setBirthday(new Date(55, 4, 14))
                .setBecomeCustomer(new Date(120, 8, 6))
                .buildCustomer();

        Customer c4 = new Customer.CustomerBuilder(Gender.FEMININ, "Popa", "Monica")
                .setMiddleName("Mirabela")
                .buildCustomer();

        Customer c5 = new Customer.CustomerBuilder(Gender.MASCULIN, "Mihailescu", "Cosmin")
                .setMiddleName("Alin")
                .setBirthday(new Date(74, 7, 12))
                .setBecomeCustomer(new Date(120, 8, 5))
                .buildCustomer();

        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        System.out.println(c4);
        System.out.println(c5);

    }
}
