package com.sda.homework.javaAdvance.all.employees;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeDemo {
    public static void main(String[] args) {

        File lista = new File("dir1/employees.txt");
        System.out.println(lista.getAbsolutePath());
        List<Employee> listaAngajati = LoadEmployees.load(lista);

/*       System.out.println(listaAngajati.size());
       for (Employee angajat : listaAngajati) {
           System.out.println(angajat.getName());
        }
*/
        //obtinem o lista sortata dupa nume si o afisam( operatie terminala collect)
        List<Employee> sortatiDupaNume = listaAngajati.stream()
                .sorted((e1, e2) -> e1.getName().compareTo(e2.getName()))
                .collect(Collectors.toList());
        System.out.println(sortatiDupaNume);


        //obtinem o lista sortata dupa salariu si apoi o afisam
        List<Employee> sortatiDupaSalariu = listaAngajati.stream()
                .sorted((e1, e2) -> e1.getSalary() - e2.getSalary())
                .collect(Collectors.toList());
        System.out.println(sortatiDupaSalariu);

        //afisam direct lista de anagajati care sunt programatori(operatie terminala forEach)
        listaAngajati.stream()
                .filter(e -> e instanceof Programmer)
                .forEach(System.out::println);

        List<Employee> salariuPeste6000 =
                listaAngajati.stream()
                        .filter(e -> (e.getSalary() >= 6000))
                        .collect(Collectors.toList());

        //afisam un salariat care are peste 6 mii salariu
        System.out.println("Angajatul: " + salariuPeste6000.get(0).getName() + " are salariul peste 6000 RON");

        //afisam numarul salariatilor care au peste 6 mii.
        System.out.println("Numarul angajatilor care au salariu peste 6000 RON este: " + salariuPeste6000.size());

        List<Programmer> programatori =
                listaAngajati.stream()
                .filter(e -> e instanceof Programmer)
                        .map(Programmer.class::cast)
                .collect(Collectors.toList());
        System.out.println("Lista programatori:\n" + programatori);

        List<SysAdmin> sysAdmin =
                listaAngajati.stream()
                        .filter(e -> e instanceof SysAdmin)
                        .map(e -> (SysAdmin) e)
                        //.map(SysAdmin.class::cast)
                        .collect(Collectors.toList());
        System.out.println("Lista SysAdmin:\n" + sysAdmin);


    }
}
