package com.sda.homework.javaAdvance.all.employees;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public final class LoadEmployees {
    private static Path path;
    private static String reader;

    private LoadEmployees() {
    }

    public static List<Employee> load(File file){
        List<Employee> employeesList = new ArrayList<Employee>();
        path = Paths.get(file.getAbsolutePath());
        try(BufferedReader br = Files.newBufferedReader(path)) {
            while((reader = br.readLine()) != null){

                String[] employeeDetails = reader.split(",");
                if(employeeDetails[0].equals("programmer")){
                    Employee employee = new Programmer(employeeDetails[1],Integer.parseInt(employeeDetails[2]),employeeDetails[3]);
                    employeesList.add(employee);
                }
                else if(employeeDetails[0].equals("sysAdmin")){
                    Employee employee = new SysAdmin(employeeDetails[1],Integer.parseInt(employeeDetails[2]),Level.valueOf(employeeDetails[3]));
                    employeesList.add(employee);
                }
                else System.out.println("Nu s-a putut inregistra angajatul, va rugam modificati datele");
            }

        } catch (IOException e) {
            System.out.println("nu s-a putut citi din fisier");
        }
        return employeesList;
    }

}
