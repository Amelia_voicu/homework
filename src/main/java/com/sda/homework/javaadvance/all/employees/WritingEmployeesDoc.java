package com.sda.homework.javaAdvance.all.employees;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class WritingEmployeesDoc {
    public static void main(String[] args) {
        String employees = "programmer,Laura,7500,python" +
                        "\nprogrammer,Razvan,3500,java" +
                        "\nsysAdmin,Claudiu,9500,L3";
        File employeesList = new File("dir1/employees.txt");

        Path path = Paths.get("dir1", "employees.txt");
        byte[] by = employees.getBytes();
        try {
            Files.write(path, by);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
