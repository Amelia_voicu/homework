package com.sda.homework.javaAdvance.all.store;

import java.util.*;
import java.util.stream.Collectors;

public final class ProductHelper {

    private ProductHelper() {
    }

    public static List<Product> getProductsByType(List<Product> list, Type type) {
        List<Product> listSorted = list.stream()
                .filter(p -> p.getType().equals(type))
                .collect(Collectors.toList());
        return listSorted;
    }

    public static void getTheMostExpensive(List<Product> list) {
        list.sort(Comparator.comparingDouble(Product::getPrice).reversed());
        System.out.println(list.get(0));
    }

    public static void getTheMostExpensiveProducts(List<Product> list, int no) {
        list.sort(Comparator.comparingDouble(Product::getPrice).reversed());
        for (int i = 0; i < list.size(); i++) {
            if (i < no) {
                System.out.println(list.get(i));
            }
        }
    }

    public static void printProductsWithASmallerPrice(List<Product> list, double price) {
        list.stream()
                .filter(p -> p.getPrice() < price)
                .forEach(System.out::println);
    }

    public static void printProductsInPriceInterval(List<Product> list, double price1, double price2) {
        if (price1 < price2)
            list.stream()
                    .filter(p -> (p.getPrice() >= price1 && p.getPrice() <= price2))
                    .forEach(System.out::println);
        else if (price1 > price2)
            list.stream()
                    .filter(p -> (p.getPrice() <= price1 && p.getPrice() >= price2))
                    .forEach(System.out::println);
        else System.out.println("Introduceti preturi diferite!");
    }

    public static void getOneProductOfASpecifiedType(List<Product> list, Type type) {
        Random rand = new Random();
        System.out.println(getProductsByType(list, type).get(rand.nextInt(getProductsByType(list, type).size())));
    }

    public static void inflatie(List<Product> list, double percent) {
        list.stream()
                .peek(p -> p.setPrice(p.getPrice() * (percent / 100) + p.getPrice()))
                .forEach(System.out::println);

    }

    public static void deleteProductsMoreExpensiveThan(List<Product> list, double price) {
        list.removeIf(p -> p.getPrice() > price);
        System.out.println(list);
    }

    public static void printVeganProductsAndOthers(List<Product> list) {
        list.stream()
                .filter(p -> p.getType().equals(Type.VEGETABLE) || p.getType().equals(Type.FRUIT))
                .forEach(System.out::println);
        list.stream()
                .filter(p -> p.getType().equals(Type.DAIRY) || p.getType().equals(Type.MEAT))
                .forEach(System.out::println);
    }

    public static double priceAverageOnProductsType(List<Product> list, Type type) {
        double sum = 0;
        Map<Type, List<Product>> productMap = makeAnewSortedList(list);
        for (Map.Entry<Type, List<Product>> entry : productMap.entrySet()) {
            if (entry.getKey().equals(type)) {
                for(Product product:entry.getValue()){
                sum+=product.getPrice();
                }
            }
            return sum/entry.getValue().size();
        }
        return 0;
    }



    public static Map<Type,List<Product>> makeAnewSortedList(List<Product> list){
        return  list.stream()
                .collect(Collectors.groupingBy(Product::getType));

    }

    public static double average(List<Product> list,Type type){
        Map<Type,Double> sumList = list.stream()
                .collect(Collectors.groupingBy(p->p.getType(),Collectors.summingDouble(p->p.getPrice())));
        return  sumList.get(type) / (list.stream()
                .filter(p->p.getType().equals(type))
                .collect(Collectors.counting()));
    }
}
