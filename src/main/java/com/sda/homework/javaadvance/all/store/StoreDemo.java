package com.sda.homework.javaAdvance.all.store;

import java.util.ArrayList;
import java.util.List;

public class StoreDemo {
    public static void main(String[] args) {

        List<Product> list = new ArrayList<>();
        list.add( new Product("Milk LaDorna",4.33,Type.DAIRY));
        list.add( new Product("Avocado",35.4,Type.FRUIT));
        list.add( new Product("Tomato",3.25,Type.VEGETABLE));
        list.add( new Product("Sour cream Napolact",3.00,Type.DAIRY));
        list.add( new Product("Yogurt Muller",1.5,Type.DAIRY));
        list.add( new Product("Chicken",22.80,Type.MEAT));
        list.add( new Product("Strawberry",26, Type.FRUIT));
        list.add( new Product("Cucumber",5.55, Type.VEGETABLE));

        //filtram produsele de un anumit tip
        System.out.println("Filtram produsele de un anumit tip:\n" + ProductHelper.getProductsByType(list,Type.VEGETABLE));

        //afisam produsul cel mai scump
        System.out.println("\nAfisam produsul cel mai scump");
        ProductHelper.getTheMostExpensive(list);

        //afisam cele mai scumpe 3 produse
        System.out.println("\nAfisam cele mai scumpe n produse");
        ProductHelper.getTheMostExpensiveProducts(list,3);

        //afisam produsele mai ieftine de un anumit pret dintr-o anumita categorie
        System.out.println("\nAfisam produsele mai ieftine de un anumit pret dintr-o anumita categorie");
        ProductHelper.printProductsWithASmallerPrice(list,10);

        //verificam daca exista produse intr-un anumit interval de pret
        System.out.println("\nVerificam daca exista produse intr-un anumit interval de pret");
        ProductHelper.printProductsInPriceInterval(list, 4, 12);

        //afisam un produs de un anumit tip
        System.out.println("\nAfisam un produs de un anumit tip");
        ProductHelper.getOneProductOfASpecifiedType(list,Type.DAIRY);

        //inflatie: toate preturile cresc cu 10%
        System.out.println("\nInflatie: toate preturile cresc cu x%");
        ProductHelper.inflatie(list,10);

        //stergem alimentele mai scumpe de o anumita suma
        System.out.println("\nStergem alimentele mai scumpe de o anumita suma");
        ProductHelper.deleteProductsMoreExpensiveThan(list,26);

        //afisam alimentele vegane si celelate separate (HARD)
        System.out.println("\nAfisam alimentele vegane si celelate separate");
        list.add( new Product("Banana",6.4,Type.FRUIT));
        ProductHelper.printVeganProductsAndOthers(list);

        //grupam dupa tip si facem media preturilor (HARD)
        System.out.println("\nGrupam dupa tip si facem media preturilor");
        System.out.println(ProductHelper.priceAverageOnProductsType(list,Type.VEGETABLE));
        System.out.println(ProductHelper.average(list,Type.VEGETABLE));

        //obtinem lista de produse grupata dupa tip (HARD)
        System.out.println("\n");

    }
}
