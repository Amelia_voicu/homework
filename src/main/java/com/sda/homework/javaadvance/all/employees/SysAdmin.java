package com.sda.homework.javaAdvance.all.employees;

public class SysAdmin extends Employee {
    private Level level;

    public SysAdmin(String name, int salary, Level level) {
        super(name, salary);
        this.level = level;
    }

    public Level getLevel() {
        return level;
    }

    @Override
    public void setSalary(int salary) {
        super.setSalary(salary);
    }

    @Override
    void workTip() {
        System.out.println(level);
    }

    @Override
    public String toString() {
        return "SysAdmin: " + getName() + " " + level + " " + getSalary();
    }
}
