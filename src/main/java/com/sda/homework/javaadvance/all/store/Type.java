package com.sda.homework.javaAdvance.all.store;

public enum Type {
    FRUIT(),
    VEGETABLE(),
    MEAT(),
    DAIRY();

    Type() { }
}
