package com.sda.homework.javaAdvance.all.employees;

public enum Level implements Comparable<Level> {
    L1("L1"),
    L2("L2"),
    L3("L3");
    private String description;

    Level(String level) {
        this.description = level;
    }
}
