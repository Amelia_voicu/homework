package com.sda.homework.javaAdvance.all.employees;

public class Programmer extends Employee {
    private String language;

    public Programmer(String name, int salary, String language) {
        super(name, salary);
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    @Override
    void workTip() {
        System.out.println(this.language);
    }


    @Override
    public void setSalary(int salary) {
        super.setSalary(salary);
    }

    @Override
    public String toString() {
        return "Programator: " + getName() + " " + language + " " + getSalary();
    }
}
