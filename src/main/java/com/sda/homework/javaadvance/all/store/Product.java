package com.sda.homework.javaAdvance.all.store;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Product {
    String name;
    Type type;
    double price;

    public Product(String name, double price, Type type) {
        this.name = name;
        this.price = price;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return name + " - " + BigDecimal.valueOf(price).setScale(2, RoundingMode.DOWN) + " RON - " + type;
    }
}
