package com.sda.homework.javaAdvance.oop.inheritance;

public class Rectangle extends Shape{
    private double length;
    private double within;

    public Rectangle(String name,double length, double within){
        super(name);
        this.length = length;
        this.within = within;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWithin() {
        return within;
    }

    public void setWithin(double within) {
        this.within = within;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public double getAria() {
        return super.getAria();
    }

    public void setAria() {
        double aria = this.length*this.within;
        super.setAria(aria);
    }

    @Override
    public double getPerimeter() {
        return super.getPerimeter();
    }

    public void setPerimeter() {
        double perimeter = 2*(this.length+this.within);
        super.setPerimeter(perimeter);
    }

    @Override
    public String toString() {
        return super.toString() +
                " length: " + length +
                "within: " + within;
    }
}
