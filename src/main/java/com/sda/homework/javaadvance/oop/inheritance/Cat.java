package com.sda.homework.javaAdvance.oop.inheritance;

public class Cat extends Animal {

    public Cat(String name){
        super(name);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName() {
        super.setName();
    }

    @Override
    public void yieldVoice() {
        super.yieldVoice();
        System.out.print(" miauuuuuuu!");
    }
}
