package com.sda.homework.javaAdvance.oop.inheritance;

public class DemoInheritance {
    public static void main(String[] args) {

        Shape shape1 = new Circle("Cerc", 30);
        Shape shape2 = new Rectangle("Dreptungi", 3, 6);

        Circle circle = new Circle("Cerc" ,3);
        circle.setPerimeter();
        circle.setAria();
        System.out.println(circle.toString());
        System.out.println(shape1.toString());

        Animal dog = new Dog("Cutu");
        dog.yieldVoice();

        Animal cat = new Cat("Flori");
        cat.yieldVoice();

    }
}
