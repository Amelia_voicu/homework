package com.sda.homework.javaAdvance.oop.inheritance;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Shape {
    private double aria = 0;
    private double perimeter = 0;
    private final String name;

    public Shape(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getAria(){
        return aria;
    }

    public void setAria(double aria) {
        this.aria = aria;
    }

    public double getPerimeter(){
        return perimeter;
    }

    public void setPerimeter(double perimeter){
        this.perimeter = perimeter;
    }

    @Override
    public String toString() {
        return "Shape: " + name +
                " Aria: " + BigDecimal.valueOf(aria).setScale(2, RoundingMode.DOWN) +
                " Perimeter: " + BigDecimal.valueOf(perimeter).setScale(2, RoundingMode.DOWN);
    }
}
