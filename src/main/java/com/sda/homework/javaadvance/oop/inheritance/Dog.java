package com.sda.homework.javaAdvance.oop.inheritance;

public class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName() {
        super.setName();
    }

    @Override
    public void yieldVoice() {
        super.yieldVoice();
        System.out.print("ham-ham!");
    }
}
