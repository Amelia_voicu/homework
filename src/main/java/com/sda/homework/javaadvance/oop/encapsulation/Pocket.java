package com.sda.homework.javaAdvance.oop.encapsulation;

public class Pocket {

    double money;

    public Pocket(double money) {
        this.money = money;
    }

    public double getMoney(double money) {
        if (money > 10 && this.money >= money) {
            this.money = this.money - money;
            return money;
        }
        System.out.println("Operatiunea nu a fost facuta!");
        return 0;
    }

    public void setMoney(double money) {
        if (money <= 0 || money > 3000)
            System.out.println("Suma introdussa nu este corecta" +
                    ", introduceti o suma cuprinsa intre 0 si 3000 RON");
        else
            this.money = this.money + money;
    }

    public void seeHowMuchMoneyIHaveInPocket(){
        System.out.println("Money left: " + money);
    }

}
