package com.sda.homework.javaAdvance.oop.encapsulation;

public enum Gender {
    FEMININ,
    MASCULIN;

    Gender() {
    }
}
