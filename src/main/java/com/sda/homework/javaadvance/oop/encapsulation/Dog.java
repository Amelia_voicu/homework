package com.sda.homework.javaAdvance.oop.encapsulation;

public class Dog {
    private String name;
    private int age;
    private Gender gender;
    private String race;
    private double weight;

    public Dog(String name, int age, Gender gender, String race, double weight) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.race = race;
        this.weight = weight;
    }

    public Dog(Gender gender, String race) {
        this("",0,gender,race,0);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age>0)
        this.age = age;
        else System.out.println("Varsta nu este introdusa corect");
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        if(weight>0.5)
        this.weight = weight;
        else System.out.println("Greutatea nu este introdusa corect");
    }

    @Override
    public String toString() {
        return "Dog name: " + name +
                ", age: " + age +
                ", gender: " + gender +
                ", race: " + race +
                ", weight: " + weight;
    }
}
