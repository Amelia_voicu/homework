package com.sda.homework.javaAdvance.oop.inheritance;

public class Circle extends Shape {
    double radius;

    public Circle(String name, double radius){
        super(name);
        this.radius = radius;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    //overload method
    public void setAria() {
        double aria = Math.PI * Math.pow(this.radius,2);
        super.setAria(aria);
    }

    //this is how override method of setAria looks
    /*@Override
    public void setAria(double aria) {
        super.setAria(aria);
    }*/

    @Override
    public double getAria() {
        return super.getAria();
    }

    //overload method
    public void setPerimeter() {
        double perimeter = 2 * Math.PI * this.radius;
        super.setPerimeter(perimeter);
    }

    @Override
    public double getPerimeter() {
        return super.getPerimeter();
    }

    @Override
    public String toString() {
        return super.toString() +
                " Radius: " + radius;
    }
}
