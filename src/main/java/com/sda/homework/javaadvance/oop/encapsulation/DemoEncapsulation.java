package com.sda.homework.javaAdvance.oop.encapsulation;

public class DemoEncapsulation {
    public static void main(String[] args) {
        Dog dog = new Dog("Jojo",2,Gender.MASCULIN,"Maidanez",7.8);
        System.out.println(dog);
        Pocket pucelusaRoz = new Pocket(50);
        pucelusaRoz.getMoney(20);
        pucelusaRoz.seeHowMuchMoneyIHaveInPocket();
        pucelusaRoz.setMoney(10);
        pucelusaRoz.getMoney(50);
        pucelusaRoz.seeHowMuchMoneyIHaveInPocket();

    }
}
