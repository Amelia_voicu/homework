package com.sda.homework.javaAdvance.oop.inheritance;

public class Animal {
    public String name;

    public Animal(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setName(){
        this.name = name;
    }

    public void yieldVoice(){
        System.out.println("Animal makes this sound: ");
    }
}
