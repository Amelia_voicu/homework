package com.sda.homework.javaAdvance.ioAndNio.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ChangingPermison {

    public static void main(String[] args) {

        Path path = Paths.get("dir1/doc.txt");
        if(Files.exists(path)){
            // printing the permissions associated with the file
            System.out.println(Files.isExecutable(path));
            System.out.println(Files.isReadable(path));
            System.out.println(Files.isWritable(path));
        }

        try {
            Files.setAttribute(Paths.get("1.txt"),"dos:redonly",true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
