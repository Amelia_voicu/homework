package com.sda.homework.javaAdvance.ioAndNio.nio;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class ReadingDoc {
    public static void main(String[] args) {

        Path path = Paths.get("dir1/doc.txt");
        String reading;
        try(BufferedReader reader = Files.newBufferedReader(path)){
            while((reading = reader.readLine()) != null){
                char[] arr = reading.toCharArray();
                for(int i=0;i<arr.length;i++)
                    if(Character.isDigit(arr[i])) {
                        System.out.println(reading);
                        break;
                    }
                }
            } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
