package com.sda.homework.javaAdvance.ioAndNio.io;
import de.svenjacobs.loremipsum.LoremIpsum;

import java.io.*;

public class WritingTxtDocument {
    public static void main(String[] args) {

        File tema = new File("tema.txt");
        BufferedWriter bw = null;
        LoremIpsum loremIpsum = new LoremIpsum();
        String text = loremIpsum.getWords(50);
        try {
            FileWriter fw = new FileWriter("tema.txt");
            bw = new BufferedWriter(fw);
            bw.write(text);
            bw.flush();
        } catch (IOException e) {
            System.out.println("Error reading from file!");
        } finally {
            if(bw!=null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    System.out.println("Error reading from file!");
                }
            }
        }

    }

}
