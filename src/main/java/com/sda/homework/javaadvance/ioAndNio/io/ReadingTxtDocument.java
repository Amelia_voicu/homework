package com.sda.homework.javaAdvance.ioAndNio.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class ReadingTxtDocument {

    public static void main(String[] args) {

        String temaText = "";
        int noWords = 0;
        int noSpecialCharacters = 0;
        int noSelectedWord = 0;

        try (FileReader fr = new FileReader("tema.txt");
             BufferedReader br = new BufferedReader(fr)) {
            while ((temaText = br.readLine()) != null) {
                System.out.println(temaText);
                noWords += countNoWords(temaText);
                noSpecialCharacters += countSpecialCharacters(temaText);
                noSelectedWord += countNoSelectedWords("et", temaText);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        } catch (IOException e) {
            System.out.println("Error reading from file!");
        }

        System.out.println(noWords);
        System.out.println(noSpecialCharacters);
        System.out.println(noSelectedWord);

    }

    public static int countNoWords(String str) {
        String[] words = str.split(" ");
        int count = words.length;
        return count;
    }

    public static int countSpecialCharacters(String str) {
        int count = 0;
        for (int i = 0; i < str.length(); i++)
            if (!(str.toUpperCase().charAt(i) >= 'A' && str.toUpperCase().charAt(i) <= 'Z') && !(str.charAt(i) >= '0' && str.charAt(i) <= '9') && (str.charAt(i) != ' '))
                count++;
        return count;
    }

    public static int countNoSelectedWords(String word, String text) {
        int count = 0;
        String newText = removeSpecialChar(text);
        String[] words = newText.split(" ");
        for (int i = 0; i < words.length; i++)
            if (words[i].equals(word))
                count++;
        return count;
    }

    public static String removeSpecialChar(String str) {
        String newString = "";
        for (int i = 0; i < str.length(); i++)
            if ((str.toUpperCase().charAt(i) >= 'A' && str.toUpperCase().charAt(i) <= 'Z') || (str.charAt(i) >= '0' && str.charAt(i) <= '9') || (str.charAt(i) == ' '))
                newString += str.charAt(i);
        return newString;
    }
}
