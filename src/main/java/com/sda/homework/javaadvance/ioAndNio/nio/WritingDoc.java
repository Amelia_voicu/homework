package com.sda.homework.javaAdvance.ioAndNio.nio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class WritingDoc {
    public static void main(String[] args) {

        String str = "Gina are multe mere" + "\nMimi are 9 pere" + "\nRodica are 3 capsuni";
        File dir = new File("dir1");
        if (!dir.exists()) {
            dir.mkdir();
        }

        /*File doc = new File("dir1/doc.txt");
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(doc))) {
            doc.createNewFile();
            writer.write("Gina are multe mere");
            writer.newLine();
            writer.write("Mimi are 9 pere");
            writer.newLine();
            writer.write("Rodica are 3 capsuni");
            writer.newLine();

        } catch (IOException e){
            System.out.println("Eroare creare fisier");
        }*/

        Path path = Paths.get("dir1", "doc.txt");
        byte[] by = str.getBytes();
        try {
            Files.write(path, by);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

