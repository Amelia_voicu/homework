package com.sda.homework.javaAdvance.ioAndNio.io;

import java.io.File;

public class ChangingPermision {
    public static void main(String[] args) {

        File tema = new File("tema.txt");
        if(tema.exists()){
            // printing the permissions associated with the file
            System.out.println("Executable: " + tema.canExecute());
            System.out.println("Readable: " + tema.canRead());
            System.out.println("Writable: "+ tema.canWrite());
        }else
            System.out.println("File not found!");

        //changing the file permissions
        tema.setExecutable(true);
        tema.setReadable(true);
        tema.setWritable(false);
        System.out.println("File permissions changed.");
    }


}
